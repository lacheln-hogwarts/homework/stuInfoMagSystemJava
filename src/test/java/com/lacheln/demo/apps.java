package com.lacheln.demo;

import java.util.Scanner;

/**
 * apps
 *
 * @author chenchao
 * @date 2023/9/15
 * @since 1.0.0
 */
public class apps {
    public static void main(String[] args) {
        System.out.println("----------欢迎来到学员信息管理系统----------");
        System.out.println("          1.根据学号查看学员信息");
        System.out.println("          2.添加学员");
        System.out.println("          3.删除学员");
        System.out.println("          4.查看所有学员信息");
        System.out.println("          5.退出系统");
        System.out.println("请输入你的选择：");
        Scanner scanner = new Scanner(System.in);
        String s = scanner.nextLine();
        switch (s) {
            case "1":
                System.out.println("请输入要查询的学员编号：");
                String s1 = scanner.nextLine();
                System.out.println("学员信息获取成功，该学员的信息为：");
                break;
            case "2":
                System.out.println("请输入学员编号：");
                String s2 = scanner.nextLine();
                System.out.println("请输入学员姓名：");
                String s3 = scanner.nextLine();
                System.out.println("请输入学员性别：");
                String s4 = scanner.nextLine();
                System.out.println("添加成功，添加的学员信息为：XXX");
                break;
            case "3":
                System.out.println("请输入想要删除的学员编号：");
                String s5 = scanner.nextLine();
                System.out.println("删除成功！");
                break;
            case "4":
                System.out.println("当前所有学员信息为：");
                break;
            case "5":
                System.out.println("系统退出成功，欢迎下次使用！");
                break;
            default:
                System.out.println("Invalid,please input (1~5)!");
        }
    }
}

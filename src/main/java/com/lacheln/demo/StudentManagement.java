package com.lacheln.demo;

/**
 * StudentManagement
 * 学员管理类
 *
 * @author chenchao
 * @date 2023/9/15
 * @since 1.0.0
 */
public class StudentManagement {

    /**
     * 添加学员方法 addStudent()
     */
    public void addStudent() {
    }

    /**
     * 删除学员方法 deleteStudent()
     */
    public void deleteStudent() {

    }
}
